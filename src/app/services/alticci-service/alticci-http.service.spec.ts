import {TestBed} from '@angular/core/testing';

import {AlticciHttpService} from './alticci-http.service';
import createSpyObj = jasmine.createSpyObj;
import {HttpClient} from '@angular/common/http';
import {of} from 'rxjs';

describe('AlticciHttpService', () => {
  let service: AlticciHttpService;
  const httpClient = createSpyObj(HttpClient, ['get']);

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: HttpClient, useValue: httpClient }
      ]
    });
    service = TestBed.inject(AlticciHttpService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
  it('should get alticci value', () => {
    httpClient.get.and.returnValue(of(1));
    service.getAlticciValue(2).subscribe(
      value => expect(value).toBe(1)
    );
  });
});
