import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AlticciHttpService {

  constructor(private readonly httpClient: HttpClient) { }

  public getAlticciValue(num: number): Observable<number> {
    return this.httpClient.get<number>(`http://${environment.baseUrl}/alticci/${num}`);
  }
}
