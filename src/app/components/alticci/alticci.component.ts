import { Component } from '@angular/core';
import { AlticciHttpService } from '../../services/alticci-service/alticci-http.service';

@Component({
  selector: 'app-alticci',
  templateUrl: './alticci.component.html',
  styleUrls: ['./alticci.component.css']
})
export class AlticciComponent {

  public alticciValue: number;

  constructor(private readonly alticciHttpService: AlticciHttpService) { }

  public getAlticciValue(num: number): void {
    this.alticciHttpService.getAlticciValue(num).subscribe(
      result => this.alticciValue = result
    );
  }

}
