import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlticciComponent } from './alticci.component';
import {HttpClient} from '@angular/common/http';
import createSpyObj = jasmine.createSpyObj;
import {AlticciHttpService} from '../../services/alticci-service/alticci-http.service';

describe('AlticciComponent', () => {
  let component: AlticciComponent;
  let fixture: ComponentFixture<AlticciComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlticciComponent ],
    })
      .compileComponents();
    TestBed.inject(AlticciHttpService);
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlticciComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
